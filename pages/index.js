/* eslint-disable max-classes-per-file */
/* eslint-disable react/no-multi-comp */

import { createMedia } from "@artsy/fresnel";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { useState } from "react";
import {
  Button,
  Container,
  Divider,
  Grid,
  Form,
  Table,
  Message,
  Rail,
  Card,
  Label,
  Header,
  Item,
  Icon,
  Image,
  List,
  Menu,
  Segment,
  Sidebar,
  Visibility,
} from "semantic-ui-react";

const { MediaContextProvider, Media } = createMedia({
  breakpoints: {
    mobile: 0,
    tablet: 768,
    computer: 1024,
  },
});

/* Heads up!
 * HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled
 * components for such things.
 */
const HomepageHeading = ({ mobile }) => (
  <Container
    style={{
      padding: "0em 0.5em",
    }}
  >
    <Header
      as="h1"
      content="Een volle, startklare Magento 2 webshop"
      inverted
      style={{
        fontSize: mobile ? "2em" : "4em",
        fontWeight: "bold",
        marginBottom: 0,
        marginTop: mobile ? "1.5em" : "2em",
      }}
    />
    <Header
      as="h2"
      content="Onze exclusieve focus op Commerce, Data & Technology is een belangrijke verklaring voor onze prestaties. Leanops heeft een obsessie met nieuwe techniek. Dat geeft jou – en ons – een heel dikke voorsprong."
      inverted
      style={{
        fontSize: mobile ? "1.3em" : "1.7em",
        fontWeight: "lighter",
        marginTop: mobile ? "0.5em" : "1.5em",
      }}
    />
  </Container>
);

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
};

/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
class DesktopContainer extends Component {
  state = {};

  hideFixedMenu = () => this.setState({ fixed: false });
  showFixedMenu = () => this.setState({ fixed: true });

  render() {
    const { children } = this.props;
    const { fixed } = this.state;

    return (
      <Media greaterThan="mobile">
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment
            inverted
            textAlign="center"
            style={{ minHeight: 450, padding: "1em 0em" }}
            vertical
          >
            <HomepageHeading />
          </Segment>
        </Visibility>

        {children}
      </Media>
    );
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
};

class MobileContainer extends Component {
  state = {};

  handleSidebarHide = () => this.setState({ sidebarOpened: false });

  handleToggle = () => this.setState({ sidebarOpened: true });

  render() {
    const { children } = this.props;
    const { sidebarOpened } = this.state;

    return (
      <Media as={Sidebar.Pushable} at="mobile">
        <Sidebar.Pushable>
          <Sidebar.Pusher dimmed={sidebarOpened}>
            <Segment
              inverted
              textAlign="center"
              style={{ minHeight: 300, padding: "1em 0em" }}
              vertical
            >
              <HomepageHeading mobile />
            </Segment>

            {children}
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </Media>
    );
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
};

const ResponsiveContainer = ({ children }) => (
  /* Heads up!
   * For large applications it may not be best option to put all page into these containers at
   * they will be rendered twice for SSR.
   */
  <MediaContextProvider>
    <DesktopContainer>{children}</DesktopContainer>
    <MobileContainer>{children}</MobileContainer>
  </MediaContextProvider>
);

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
};

const HomepageLayout = () => (
  <ResponsiveContainer>
    <Segment style={{ padding: "6em 0em" }} vertical>
      <Grid stackable columns={2}>
        <Grid.Column style={{ padding: "0em 6em" }}>
          <p style={{ fontSize: "1.33em" }}>
            Magento 2 biedt veel kansen. Het platform is open source, krachtig
            en biedt de perfecte basis voor online groei. Maar ontwikkelen in
            Magento 2 blijkt vaak kostbaar te zijn; prijzen van tienduizenden
            euro's zijn eerder regel dan uitzondering.
          </p>
          <p style={{ fontSize: "1.33em" }}>
            Leanops introduceert daarom Essentials. Essentials is een
            instapklare en schaalbare webshop voor een vaste maandelijkse prijs.
            Essentials combineert de beste resultaten uit de praktijk en is
            ontwikkeld op het krachtige Magento 2 platform.
          </p>
          <p style={{ fontSize: "1.33em" }}>
            Essentials drukt investeringskosten, versnelt de time to market en
            geeft veel waarde voor de investering. Bovendien behoud je alle
            doorontwikkelingsmoglijkheden die Magento 2 biedt.
          </p>
          <p style={{ fontSize: "1.33em" }}>
            Wij staan voor je klaar bij iedere stap van je e-commerce reis. Niet
            alleen als er problemen zijn, maar ook voor leuke uitdagingen,
            advies, groeiplannen en meedenken over slimme samenwerkingen. Je
            kunt via meerdere kanalen om hulp vragen. Of het nu chat, mail of
            telefoon is, ons supportteam staat aan je zijde. We staan in de
            nacht voor je op om je problemen direct op te lossen zodat je
            omzetstroom kan blijven lopen.
          </p>
          <Form>
            <Form.Input
              style={{ width: "30em" }}
              action="Submit"
              placeholder="E-mail adres"
            />

            <Header.Subheader style={{ color: "#ccc", padding: "2em 0em" }}>
              We zullen je niet spammen. We gebruiken jouw e-mail adres om een
              demo account aan te maken. Je ontvangt de inloggegevens per email
              en je kunt direct zonder onze tussenkomst de demo zelfstandig
              bekijken.
            </Header.Subheader>
          </Form>
        </Grid.Column>
        <Grid.Column>
          <Image src="/images/57270c1.png" fluid />
        </Grid.Column>
      </Grid>
    </Segment>

    <Container text>
      <Segment style={{ padding: "6em 0em" }} vertical>
        <Header as="h3" style={{ fontSize: "2em" }}>
          Ervaar de kracht van Essentials op het Magento2 platform zonder torenhoge facturen te hoeven betalen
        </Header>
        <p style={{ fontSize: "1.33em" }}>
          Het is 2015. Steven Veeneman, die net zijn propedeuse heeft gehaald
          aan de Hanzehogeschool in Groningen, ziet grote online kansen liggen
          voor ontelbare ondernemingen. Deels uit frustratie, deels uit
          nieuwsgierigheid. Het viel op dat webshops regelmatig te kampen hebben
          met verouderde technieken. De creatieve, slimme Achterhoeker van nog
          geen twintig jaar oud, kijkt niet lang toe. “De grootste ondernemingen
          hadden het totaal niet voor elkaar. Er was zoveel mogelijk om die
          bedrijven hoog in Google te krijgen. Ik wilde ze hierbij helpen en hen
          meer uit hun onderneming laten halen,” licht hij toe.
        </p>
        <p style={{ fontSize: "1.33em" }}>
          In het begin werkte Steven alleen op basis van ‘no cure, no pay’. “Ik
          plaatse dan vaak een bestelling bij een potentiële klant om de
          customer journey te analyseren, waarnaar ik een email verstuurde met:
          ‘wilt u binnen drie maanden meer omzet genereren dan directe
          concurrenten? Als het lukt, sturen we achteraf een factuur. Lukt het
          niet, dan is het ‘no cure, no pay’.’ En het lukte.” Het principe wekte
          vertrouwen bij de ondernemingen die hij mailde. Hoewel Steven aan de
          telefoon sprak over ‘we’, werkte hij alleen. Maar dat maakte niets
          uit: vanuit zijn zolderkamer kwam hij zijn belofte na en liet hij
          bedrijven groeien. Zelf groeide hij ook. Leanops is een groeiende en
          vooruitstrevende organisatie. Dat blijkt uit hun manier van denken.
          Steven: “We willen de beste zijn en blijven in ons vakgebied. Iedereen
          binnen ons team is gek op experimenteren en de ideeën vliegen je
          altijd om de oren. Dat is leuk en dat geeft ons allemaal energie.
          Belangrijk is ook dat we bij alles wat we doen, niet bang zijn om
          fouten te maken.” Maar voor Leanops is een gezonde mindset en gezond
          leven net zo belangrijk voor de groei van het bedrijf en de individu.
          Volgens Steven gaat het ook om een ‘healthy mind’. “Sporten is
          belangrijk, net als vrije tijd en sociale contacten.”
        </p>
        <p style={{ fontSize: "1.33em" }}>
          Bij alles wat Leanops doet, streeft het naar het technisch hoogst
          haalbare. Maar ook naar het hoogst haalbare als het gaat om
          effectiviteit en winstgevendheid. “Dat kan alleen als we de business
          van onze klanten begrijpen. Volledige transparantie: alles wat we
          zeggen, mag je testen. Alleen op die manier kunnen we onze klant laten
          groeien en de business verbeteren.” Verwacht dus ook kritische vragen
          van onze kant, zodat we het beste in elkaar naar boven kunnen halen.
          Leanops zoekt daarom continu naar ondernemingen en sterke merken die
          commerce, data & technology zien als een cruciale driver van hun
          business. Ambitieuze ondernemers die klaar zijn voor de volgende stap.
          Als deze bedrijven een partner zoeken die hen naar een volgend niveau
          kan brengen, is Leanops een logische keus.
        </p>
        <Card>
          <Card.Content>
            <Image
              floated="right"
              size="mini"
              src="/images/avatar/large/steve.jpg"
            />
            <Card.Header>Steven Veeneman</Card.Header>
            <Card.Meta>Founder of Leanops</Card.Meta>
          </Card.Content>
        </Card>
      </Segment>
    </Container>
  </ResponsiveContainer>
);

export default HomepageLayout;
